#include "BossCannon.h"
#include "BossCannon.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "BossBullet.h"
#include "Tank.h"




//コンストラクタ
BossCannon::BossCannon(GameObject * parent)
	:GameObject(parent, "BossCannon"), hModel_(-1)
{
}

//デストラクタ
BossCannon::~BossCannon()
{
}

//初期化
void BossCannon::Initialize()
{
	hModel_ = Model::Load("tankhV3.fbx");
	assert(hModel_ >= 0); //assertは大事
}

//更新
void BossCannon::Update()
{
	

	//敵を探して、いなかったら 
	if (FindObject("Tank") == nullptr)
	{

	}
	else
	{
		XMVECTOR flont = { 0,0,1,0 };
		XMMATRIX mat;

		Tank* pTank;
		pTank = (Tank*)FindObject("Tank");

		mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
		flont = XMVector3TransformCoord(flont, mat);

		XMVECTOR  mov = pTank->GetPosition() - transform_.position_;
		XMVECTOR  interval = pTank->GetPosition() - transform_.position_;

		mov = XMVector3Normalize(mov);
		flont = XMVector3Normalize(flont);
		float dot = XMVector3Dot(flont, mov).vecX;	//内積を求める	
		float angle = acos(dot);

		angle = XMConvertToDegrees(angle);

		XMMATRIX ro;
		ro = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	

		transform_.rotate_.vecY += angle / 50;

		float longitud = XMVector3Length(interval).vecX; //ベクトルの長さ


		if (Visual > angle && longitud < distance && FindObject("Timer") == nullptr) //視界に入ったら玉を出す
		{
			static int s_nTime;
			if (++s_nTime == 9)
			{
				s_nTime = -27;

				//発射
				BossBullet* pBossBullet = Instantiate<BossBullet>(GetParent()->GetParent()->GetParent());

				XMVECTOR shotPos = Model::GetBonePosition(hModel_, "BShot");//砲台の先の骨を探す
				XMVECTOR CannonRoot = Model::GetBonePosition(hModel_, "BCRoot");//砲台の根っこの骨を探す

				pBossBullet->Shot(shotPos, shotPos - CannonRoot);  //shotPos - cannonRootでcannonRootから見たshotPosとの距離がわかる

			}


		}
	}

}



//描画
void BossCannon::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void BossCannon::Release()
{
}

