#pragma once
#include "Engine/GameObject.h"


//◆◆◆を管理するクラス
class BossCannon : public GameObject
{
private:
	int hModel_;    //モデル番号
	const int Visual = 90; //視野の角度
	const int distance = 70; //視野の長さ

public:
	//コンストラクタ
	BossCannon(GameObject* parent);

	//デストラクタ
	~BossCannon();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;


	//描画
	void Draw() override;

	//開放
	void Release() override;

};