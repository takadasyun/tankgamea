#include "Bullet.h"
#include "Ground.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/SphereCollider.h"


//コンストラクタ
Bullet::Bullet(GameObject * parent)
	:GameObject(parent, "Bullet"), hModel_(-1), move_{ 0.0f, 0.0f, 0.0f, 0.0f }
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);

	//衝突範囲(コライダー)
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.8f); //(XMVectorSet 位置(X,Y,Z,?),大きさ)
	AddCollider(collision);
}

//更新
void Bullet::Update()
{	
		transform_.position_ += move_;  //弾を奥に動かす
		
		move_.vecY -= Gravity; //重力を加える

		if (transform_.position_.vecY <= -30)  //弾がある程度、奥に行ったら消える
		{
			KillMe();
		}
}

//描画
void Bullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}

//発射
void Bullet::Shot(XMVECTOR position, XMVECTOR direction)
{
	transform_.position_ = position; //発射する場所をセットする
	move_ = XMVector3Normalize(direction) * SPEED; //発射する方向をセットする
	                                              //単位ベクトルしたのスカラー倍（0.5）してある
												  

    //XMVECTOR move_ = direction * 0.5f;
}

//何かに当たった
void Bullet::OnCollision(GameObject * pTarget) // (GameObject * 当たるもの)
{
	//弾に当たったとき

	if (pTarget->GetObjectName() == "EnemyBullet" || pTarget->GetObjectName() == "BossBullet")
	{
		KillMe();
		pTarget->KillMe();
	}
}