#pragma once
#include "Engine/GameObject.h"

//◆◆◆を管理するクラス
class BulletFar : public GameObject
{
	const float SPEED = 0.5f;
	const float Gravity = 0.0005; // 重力

	int hModel_;    //モデル番号
	XMVECTOR move_; //移動ベクトル

public:
	//コンストラクタ
	BulletFar(GameObject* parent);

	//デストラクタ
	~BulletFar();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//弾を発射させる（初期設定）
	//引数：position　発射位置
	//引数：direction　発射方向
	//戻り値：なし
	void Shot(XMVECTOR position, XMVECTOR direction);

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;
};