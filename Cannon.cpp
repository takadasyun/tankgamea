#include "Cannon.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Bullet.h"
#include "BulletFar.h"




//コンストラクタ
Cannon::Cannon(GameObject * parent)
	:GameObject(parent, "Cannon"), hModel_(-1)
{
}

//デストラクタ
Cannon::~Cannon()
{
}

//初期化
void Cannon::Initialize()
{
	hModel_ = Model::Load("TankPCv2.fbx");
	assert(hModel_ >= 0); //assertは大事

}

//更新
void Cannon::Update()
{
	int Move = 1;      //砲身の動作速度
	int UpLimit = -5; //砲身が上に行き過ぎないようにする
	int DownLimit = 5; //砲身が下に行き過ぎないようにする


	//砲身が行き過ぎていないかどうかの処理
	if (transform_.rotate_.vecX < DownLimit && transform_.rotate_.vecX > UpLimit)
	{
		if (Input::IsKey(DIK_UP))
		{
			transform_.rotate_.vecX -= Move;
		}

		if (Input::IsKey(DIK_DOWN))
		{
			transform_.rotate_.vecX += Move;
		}
	}
	//下に行き過ぎていた場合
	else if (transform_.rotate_.vecX >= DownLimit)
	{
		transform_.rotate_.vecX -= Move;
	}
	//上に行き過ぎていた場合
	else if (transform_.rotate_.vecX <= UpLimit)
	{
		transform_.rotate_.vecX += Move;
	}

	//発射
	if (Input::IsKey(DIK_RCONTROL) || Input::IsKey(DIK_LCONTROL))
	{
		if (Input::IsKeyDown(DIK_SPACE))
		{
			BulletFar* pBulletFar = Instantiate<BulletFar>(GetParent()->GetParent()->GetParent());// (FindObject("PlayScene"))でもいいが、
																			//  いちいち探すのは、しんどい

			XMVECTOR shotPos = Model::GetBonePosition(hModel_, "TPShot");//砲台の先の骨を探す
			XMVECTOR CannonRoot = Model::GetBonePosition(hModel_, "TPRoot");//砲台の根っこの骨を探す

			pBulletFar->Shot(shotPos, shotPos - CannonRoot);  //shotPos - cannonRootでcannonRootから見たshotPosとの距離がわかる
		}
	}
	else if (Input::IsKeyDown(DIK_SPACE)) //SPACEキーで
	{

		Bullet* pBullet = Instantiate<Bullet>(GetParent()->GetParent()->GetParent());// (FindObject("PlayScene"))でもいいが、
																		//  いちいち探すのは、しんどい

		XMVECTOR shotPos = Model::GetBonePosition(hModel_, "TPShot");//砲台の先の骨を探す
		XMVECTOR CannonRoot = Model::GetBonePosition(hModel_, "TPRoot");//砲台の根っこの骨を探す

		pBullet->Shot(shotPos, shotPos - CannonRoot);  //shotPos - cannonRootでcannonRootから見たshotPosとの距離がわかる

		//pBullet->Shot(shotPos, XMVectorSet(0.1,0,0,0));   //探した骨から玉が出るようにする

	}
}



//描画
void Cannon::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Cannon::Release()
{
}


