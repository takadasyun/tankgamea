#include "Enemy.h"
#include "Tank.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "Ground.h"
#include "EnemyCannon.h"
#include "TigerCannon.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//コンストラクタ
Enemy::Enemy(GameObject * parent)
	:GameObject(parent, "Enemy"), hModel_(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	hModel_ = Model::Load("TankFVB.fbx");
	assert(hModel_ >= 0);
	Instantiate<EnemyCannon>(this);
	int a = 3;


	//初期位置
	//戦車に当たらないように配置する
	while (1)
	{
		transform_.position_.vecX = rand() % 80 - 40;   // ランダムでX軸を設定する rand(出したい数−最小値)
		transform_.position_.vecZ = rand() % 80 - 40;

		if (transform_.position_.vecX > 8 || transform_.position_.vecX < -8 || transform_.position_.vecZ < -8 || transform_.position_.vecZ > 8)
		{
			break;
		}

	}

	//衝突範囲(コライダー)
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0.4, 0, 0), 0.6f); //(XMVectorSet 位置(X,Y,Z,?),大きさ)
	AddCollider(collision);

}
//更新
void Enemy::Update()
{
	//移動処理
	Move();

	//高さを地面に合わせる
	FitHeightToGround();

}

//高さを地面に合わせる
void Enemy::FitHeightToGround()
{
	//地面に添わせる
	Ground* pGround = (Ground*)FindObject("Ground");    //ステージオブジェクトを探す
	int hGroundModel = pGround->GetModelHandle();    //モデル番号を取得

	RayCastData data;
	data.start = transform_.position_; //レイの発射位置
	data.start.vecY = 0.0f;
	data.dir = XMVectorSet(0, -1, 0, 0); //レイの方向
	Model::RayCast(hGroundModel, &data); //レイを発射

	//レイが当たったら
	if (data.hit)
	{
		//その分位置を下げる
		transform_.position_.vecY = -data.dist;//data.distは発射位置からレイが当たった所までの距離
	}
	else
	{
		//ステージ外に出ないようにする処理
		int checkX, checkZ;
		double Adjust = 0.3f;//戻りすぎたりしないための調整

		//斜めに動かした時にステージの周りに沿って動かしたいため
		//各方向の処理を入れる

		//左の処理
		checkX = (int)(transform_.position_.vecX - Adjust);//めり込まなくする
		checkZ = (int)transform_.position_.vecZ;
		transform_.position_.vecX = checkX + 1 + Adjust;//直前にいた位置に戻る


		//右の処理
		checkX = (int)(transform_.position_.vecX + Adjust);//めり込まなくする
		checkZ = (int)transform_.position_.vecZ;
		transform_.position_.vecX = checkX - 1 - Adjust;//直前にいた位置に戻る


		//奥の処理
		checkX = (int)transform_.position_.vecX;
		checkZ = (int)(transform_.position_.vecZ + Adjust);//めり込まなくする
		transform_.position_.vecZ = checkZ - 1 - Adjust;//直前にいた位置に戻る


		//前の処理
		checkX = (int)transform_.position_.vecX;
		checkZ = (int)(transform_.position_.vecZ - Adjust);//めり込まなくする
		transform_.position_.vecZ = checkZ + 1 + Adjust;//直前にいた位置に戻る

	}
}

//移動処理
void Enemy::Move()
{
	if (FindObject("Tank") == nullptr)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GameoverScene);
	}
	else
	{
		XMVECTOR flont = { 0,0,1,0 };
		XMMATRIX mat;

		Tank* pTank;
		pTank = (Tank*)FindObject("Tank");

		mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
		flont = XMVector3TransformCoord(flont, mat);

		XMVECTOR  mov = pTank->GetPosition() - transform_.position_;
		XMVECTOR  interval = pTank->GetPosition() - transform_.position_;

		mov = XMVector3Normalize(mov);
		flont = XMVector3Normalize(flont);
		float dot = XMVector3Dot(flont, mov).vecX;	//内積を求める	
		float angle = acos(dot);

		angle = XMConvertToDegrees(angle);


		XMVECTOR move = { 0.0f,0.0f,0.15f,0.0f };
		XMVECTOR back = { 0.0f,0.0f,-0.1f,0.0f };

		XMMATRIX ro;
		ro = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
		move = XMVector3TransformCoord(move, ro);
		back = XMVector3TransformCoord(back, ro);

		transform_.rotate_.vecY += angle / 50;


		transform_.position_ += move;//前に行く処理
	}
}

//描画
void Enemy::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}

//何かに当たった
void Enemy::OnCollision(GameObject * pTarget) // (GameObject * 当たるもの)
{
	//弾に当たったとき
	if (pTarget->GetObjectName() == "Bullet"|| pTarget->GetObjectName() == "BulletFar")
	{
		KillMe();
	}

}
