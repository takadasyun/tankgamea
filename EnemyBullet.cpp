#include "EnemyBullet.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/SphereCollider.h"


//コンストラクタ
EnemyBullet::EnemyBullet(GameObject * parent)
	:GameObject(parent, "EnemyBullet"), hModel_(-1), move_{ 0.0f, 0.0f, 0.0f, 0.0f }
{
}

//デストラクタ
EnemyBullet::~EnemyBullet()
{
}

//初期化
void EnemyBullet::Initialize()
{
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);
	//transform_.position_.vecX += -5;

	//衝突範囲(コライダー)
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.8f); //(XMVectorSet 位置(X,Y,Z,?),大きさ)
	AddCollider(collision);
}

//更新
void EnemyBullet::Update()
{
	transform_.position_ += move_;  //弾を奥に動かす

	int gra = rand() % 2;

	switch (gra)
	{
	case 0:
		move_.vecY -= Gravity; //重力を加える
		break;

	case 1:
		move_.vecY -= Gravity2; //重力を加える
		break;

	default:
		move_.vecY -= Gravity; //重力を加える
		break;
	}


	if (transform_.position_.vecY <= -30)  //弾がある程度、奥に行ったら消える
	{
		KillMe();
	}
}

//描画
void EnemyBullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void EnemyBullet::Release()
{
}
//発射
void EnemyBullet::Shot(XMVECTOR position, XMVECTOR direction)
{
	transform_.position_ = position; //発射する場所をセットする
	move_ = XMVector3Normalize(direction) * SPEED; //発射する方向をセットする
												  //単位ベクトルしたのスカラー倍（0.5）してある

}
