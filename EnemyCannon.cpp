#include "EnemyCannon.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "EnemyBullet.h"
#include "Tank.h"




//コンストラクタ
EnemyCannon::EnemyCannon(GameObject * parent)
	:GameObject(parent, "EnemyCannon"), hModel_(-1)
{
}

//デストラクタ
EnemyCannon::~EnemyCannon()
{
}

//初期化
void EnemyCannon::Initialize()
{
	hModel_ = Model::Load("TankFVC.fbx");
	assert(hModel_ >= 0); //assertは大事
}

//更新
void EnemyCannon::Update()
{

	//発射
	XMVECTOR flont = { 0,0,1,0 };
	XMMATRIX mat;
	

	if (FindObject("Tank") == nullptr)
	{
		
	}
	else
	{
		XMVECTOR flont = { 0,0,1,0 };
		XMMATRIX mat;

		Tank* pTank;
		pTank = (Tank*)FindObject("Tank");

		mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
		flont = XMVector3TransformCoord(flont, mat);

		XMVECTOR  mov = pTank->GetPosition() - transform_.position_;
		XMVECTOR  interval = pTank->GetPosition() - transform_.position_;

		mov = XMVector3Normalize(mov);
		flont = XMVector3Normalize(flont);
		float dot = XMVector3Dot(flont, mov).vecX;	//内積を求める	
		float angle = acos(dot);

		angle = XMConvertToDegrees(angle);


		

		XMMATRIX ro;
		ro = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
		

		transform_.rotate_.vecY += angle / 50;

		float longitud = XMVector3Length(interval).vecX; //ベクトルの長さ


		if (Visual > angle && longitud < distance &&FindObject("Timer") == nullptr) //視界に入ったら玉を出す
		{
			static int s_nTime;
			if (++s_nTime == 9)
			{
				s_nTime = -36;
			
				EnemyBullet* pEnemyBullet = Instantiate<EnemyBullet>(GetParent()->GetParent());// (FindObject("PlayScene"))でもいいが、
																				//  いちいち探すのは、しんどい

				XMVECTOR shotPos = Model::GetBonePosition(hModel_, "TankShotE");//砲台の先の骨を探す
				XMVECTOR EnemyCannonRoot = Model::GetBonePosition(hModel_, "TankRootE");//砲台の根っこの骨を探す

				pEnemyBullet->Shot(shotPos, shotPos - EnemyCannonRoot);  //shotPos - EnemyCannonRootでEnemyCannonRootから見たshotPosとの距離がわかる


			}
			
			
		}
	}
	

}



//描画
void EnemyCannon::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void EnemyCannon::Release()
{
}

