#pragma once
#include "Engine/GameObject.h"

//◆◆◆を管理するクラス
class EnemyCannon : public GameObject
{
	int hModel_;    //モデル番号
	const int Visual = 70; //視野の角度
	const int distance = 50; //視野の長さ

public:
	//コンストラクタ
	EnemyCannon(GameObject* parent);

	//デストラクタ
	~EnemyCannon();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;


	//描画
	void Draw() override;

	//開放
	void Release() override;
};