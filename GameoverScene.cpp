#include "Gameoverscene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"

//コンストラクタ
GameoverScene::GameoverScene(GameObject * parent)
	: GameObject(parent, "GameoverScene"), hPict_(-1)
{
}

//初期化
void GameoverScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("GameoverLogo.png");
	assert(hPict_ >= 0);


}

//更新
void GameoverScene::Update()
{
	if (Input::IsKey(DIK_RETURN))
	{
		
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_StartScene);
		
	}
}

//描画
void GameoverScene::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void GameoverScene::Release()
{
}