#include "ManualScene1.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"

//コンストラクタ
ManualScene1::ManualScene1(GameObject * parent)
	: GameObject(parent, "ManualScene1"), hPict_1(-1), hPict_2(-1), hPict_3(-1)
{
}

//初期化
void ManualScene1::Initialize()
{
	//画像データのロード
	hPict_1 = Image::Load("ManualLogo1.png");
	assert(hPict_1 >= 0);
	transform_.position_.vecX = -0.45;
	transform_.position_.vecY += 0.45;

	hPict_2 = Image::Load("ManualLogo2.png");
	assert(hPict_2 >= 0);
	trans1.position_.vecX = 0.45; 
	trans1.position_.vecY += 0.45;

	hPict_3 = Image::Load("ManualLogo3.png");
	assert(hPict_3 >= 0);
	trans2.position_.vecY -= 0.45;

	

}

//更新
void ManualScene1::Update()
{
	if (Input::IsKey(DIK_RETURN))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_Play);
	}
}

//描画
void ManualScene1::Draw()
{
	Image::SetTransform(hPict_1, transform_);
	Image::Draw(hPict_1);

	Image::SetTransform(hPict_2, trans1);
	Image::Draw(hPict_2);

	Image::SetTransform(hPict_3, trans2);
	Image::Draw(hPict_3);
}

//開放
void ManualScene1::Release()
{
}