#pragma once
#include "Engine/GameObject.h"


//説明シーンを管理するクラス
class ManualScene1 : public GameObject
{
	int hPict_1;    //画像番号
	int hPict_2;    //画像番号
	int hPict_3;    //画像番号

	Transform  trans1;
	Transform  trans2;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ManualScene1(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};