#include "ManualScene2.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"

//コンストラクタ
ManualScene2::ManualScene2(GameObject * parent)
	: GameObject(parent, "ManualScene2"), hPict_(-1)
{
}

//初期化
void ManualScene2::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Warning.png");
	assert(hPict_ >= 0);


}

//更新
void ManualScene2::Update()
{
	if (Input::IsKey(DIK_RETURN))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_Play);
	}


	if (Input::IsKey(DIK_SPACE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_ManualScene1);
	}
}

//描画
void ManualScene2::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void ManualScene2::Release()
{
}