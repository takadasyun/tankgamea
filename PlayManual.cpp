#include "PlayManual.h"
#include "Engine/Image.h"

//コンストラクタ
PlayManual::PlayManual(GameObject * parent)
	: GameObject(parent, "PlayManual"), hPict_1(-1), hPict_2(-1), hPict_3(-1)
{
}

//初期化
void PlayManual::Initialize()
{

	//画像データのロード
	hPict_1 = Image::Load("ManualLogo1.png");
	assert(hPict_1 >= 0);
	transform_.position_ = { -0.75,0.75,0,0 };
	transform_.scale_ = { 0.5f,0.5f,0.5f,1 };

	hPict_2 = Image::Load("ManualLogo2.png");
	assert(hPict_2 >= 0);
	trans1_.position_ = { -0.25,0.75f,0,0 };
	trans1_.scale_ = { 0.5f,0.5f,0.5f,1 };

	hPict_3 = Image::Load("ManualLogo3.png");
	trans2_.position_ = { 0.25,0.75f,0,0 };
	trans2_.scale_ = { 0.5f,0.5f,0.5f,1 };


}

//更新
void PlayManual::Update()
{

}

//描画
void PlayManual::Draw()
{
	Image::SetTransform(hPict_1, transform_);
	Image::Draw(hPict_1);

	Image::SetTransform(hPict_2, trans1_);
	Image::Draw(hPict_2);

	Image::SetTransform(hPict_3, trans2_);
	Image::Draw(hPict_3);
}

//開放
void PlayManual::Release()
{
}
