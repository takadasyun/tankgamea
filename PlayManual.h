#pragma once
#include "Engine/GameObject.h"


//説明の表示を管理するクラス
class PlayManual : public GameObject
{
	int hPict_1;    //画像番号
	int hPict_2;    //画像番号
	int hPict_3;    //画像番号

	Transform  trans1_;
	Transform  trans2_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayManual(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
