#include "PlayScene.h"
#include "Tank.h"
#include "Cannon.h"
#include "Ground.h"
#include "Enemy.h"
#include "Tiger.h"
#include "BossEnemy.h"
#include "PlayManual.h"

//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{

	

	Instantiate<Tank>(this);


	for (int i = 0; i < 1; i++)
	{
		Instantiate<Enemy>(this);
	}

	   // Instantiate<Tiger>(this);

		//Instantiate<BossEnemy>(this);

		Instantiate<PlayManual>(this);

		Instantiate<Ground>(this);
		
}

//更新
void PlayScene::Update()
{
	
}

//描画
void PlayScene::Draw()
{
	
}

//開放
void PlayScene::Release()
{
}
