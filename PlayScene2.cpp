#include "PlayScene2.h"
#include "Tank.h"
#include "Cannon.h"
#include "Ground.h"
#include "Enemy.h"
#include "Tiger.h"
#include "BossEnemy.h"
#include "PlayManual.h"

//コンストラクタ
PlayScene2::PlayScene2(GameObject* parent)
	: GameObject(parent, "PlayScene2")
{
}

//初期化
void PlayScene2::Initialize()
{



	Instantiate<Tank>(this);


	/*for (int i = 0; i < 1; i++)
	{
		Instantiate<Enemy>(this);
	}*/

	Instantiate<Tiger>(this);

	Instantiate<BossEnemy>(this);

	Instantiate<PlayManual>(this);

	Instantiate<Ground>(this);

}

//更新
void PlayScene2::Update()
{

}

//描画
void PlayScene2::Draw()
{

}

//開放
void PlayScene2::Release()
{
}
