#include "PlayScene3.h"
#include "Tank.h"
#include "Cannon.h"
#include "Ground.h"
#include "Enemy.h"
#include "BossEnemy.h"


//コンストラクタ
PlayScene3::PlayScene3(GameObject * parent)
	: GameObject(parent, "PlayScene3")
{
}

//初期化
void PlayScene3::Initialize()
{
	Instantiate<Tank>(this);
	Instantiate<Ground>(this);

	for (int i = 0; i < 3; i++)
	{
		Instantiate<Enemy>(this);
	}

	for (int i = 0; i < 3; i++)
	{
		Instantiate<BossEnemy>(this);
	}
}

//更新
void PlayScene3::Update()
{
}

//描画
void PlayScene3::Draw()
{
}

//開放
void PlayScene3::Release()
{
}
