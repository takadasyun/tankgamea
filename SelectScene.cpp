#include "SelectScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"

//コンストラクタ
SelectScene::SelectScene(GameObject * parent)
	: GameObject(parent, "SelectScene"), hPict_(-1)
{
}

//初期化
void SelectScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Level.png");
	assert(hPict_ >= 0);


}

//更新
void SelectScene::Update()
{
	if (Input::IsKey(DIK_1)|| Input::IsKey(DIK_NUMPAD1))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_Play);
	}

	if (Input::IsKey(DIK_2) || Input::IsKey(DIK_NUMPAD2))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_Play2);
	}

	if (Input::IsKey(DIK_3) || Input::IsKey(DIK_NUMPAD3))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_Play3);
	}

}

//描画
void SelectScene::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void SelectScene::Release()
{
}