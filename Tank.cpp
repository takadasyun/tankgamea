#include "Tank.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"
#include "Engine/SceneManager.h"
#include "Engine/SphereCollider.h"
#include "Ground.h"
#include "Turret.h"
#include "EnemyBullet.h"




//コンストラクタ
Tank::Tank(GameObject * parent)
	:GameObject(parent, "Tank"), hModel_(-1)
{
}

//デストラクタ
Tank::~Tank()
{
}

//初期化
void Tank::Initialize()
{
	hModel_ = Model::Load("TankPBv1.fbx");
	assert(hModel_ >= 0); //assertは大事
	Instantiate<Turret>(this);

	transform_.position_.vecX = rand() % 100 - 50;   // ランダムでX軸を設定する rand(出したい数−最小値)
	transform_.position_.vecZ = rand() % 100 - 50;

	//衝突範囲(コライダー)
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 1, 0, 0), 1.5f); //(XMVectorSet 位置(X,Y,Z,?),大きさ)
	AddCollider(collision);
}

//更新
void Tank::Update()
{
	Move();

	FitHeightToGround();
	
	if (FindObject("Enemy") == nullptr )
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_Play2);
	}

	
}

//高さを地面に合わせる
void Tank::FitHeightToGround()
{
	//地面に添わせる
	Ground* pGround = (Ground*)FindObject("Ground");    //ステージオブジェクトを探す
	int hGroundModel = pGround->GetModelHandle();    //モデル番号を取得

	RayCastData data;
	data.start = transform_.position_; //レイの発射位置
	data.start.vecY = 0.0f;
	data.dir = XMVectorSet(0, -1, 0, 0); //レイの方向
	Model::RayCast(hGroundModel, &data); //レイを発射

										 //レイが当たったら
	if (data.hit)
	{
		//その分位置を下げる
		transform_.position_.vecY = -data.dist;//data.distは発射位置からレイが当たった所までの距離
	}
	else
	{
		//マップ外に出ないようにする処理
		int checkX, checkZ;
		double Adjust = 0.3f;//戻りすぎたりしないための調整

		//斜めに動かした時に壁に沿って動かしたいため
		//各方向の処理を入れる

		//左の処理
		checkX = (int)(transform_.position_.vecX - Adjust);//めり込まなくする
		checkZ = (int)transform_.position_.vecZ;
		transform_.position_.vecX = checkX + 1 + Adjust;//直前にいた位置に戻る
		

		//右の処理
		checkX = (int)(transform_.position_.vecX + Adjust);//めり込まなくする
		checkZ = (int)transform_.position_.vecZ;
		transform_.position_.vecX = checkX -1- Adjust;//直前にいた位置に戻る
		

		//奥の処理
		checkX = (int)transform_.position_.vecX;
		checkZ = (int)(transform_.position_.vecZ + Adjust);//めり込まなくする
		transform_.position_.vecZ = checkZ -1- Adjust;//直前にいた位置に戻る
		

		//前の処理
		checkX = (int)transform_.position_.vecX;
		checkZ = (int)(transform_.position_.vecZ - Adjust);//めり込まなくする
		transform_.position_.vecZ = checkZ + 1 + Adjust;//直前にいた位置に戻る

	}
}

//移動処理
void Tank::Move()
{
	XMVECTOR move = { 0.0f,0.0f,0.2f,0.0f };
	XMVECTOR back = { 0.0f,0.0f,-0.2f,0.0f };

	XMMATRIX ro;
	ro = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move = XMVector3TransformCoord(move, ro);
	back = XMVector3TransformCoord(back, ro);

	if (Input::IsKey(DIK_W)) //wキーで
	{
		transform_.position_ += move;//前に行く処理
	}

	if (Input::IsKey(DIK_S)) //sキーで
	{
		transform_.position_ += back;//後ろに行く処理
	}

	if (Input::IsKey(DIK_D)) //Dキーで
	{
		transform_.rotate_.vecY += 1; //回転
	}

	if (Input::IsKey(DIK_A)) //Aキーで
	{
		transform_.rotate_.vecY -= 1; //回転
	}

	int a = 0;

	if (Input::IsKey(DIK_Q))
	{
		if (a == 0)
		{
			a += 1;
		}
	}

	
	//カメラの移動
	Camera::SetTarget(transform_.position_ + XMVectorSet(0, 8, 0, 0));//カメラが映す場所

	XMVECTOR camVec = { 0, 9.0f, -18.0f };
	

	camVec = XMVector3TransformCoord(camVec, ro);

	Camera::SetPosition(transform_.position_ + camVec);//カメラがある位置
}

//描画
void Tank::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Tank::Release()
{
}

//何かに当たった
void Tank::OnCollision(GameObject * pTarget) // (GameObject * 当たるもの)
{
	//弾に当たったとき
	if (pTarget->GetObjectName() == "Enemy"|| pTarget->GetObjectName() == "EnemyBullet" || pTarget->GetObjectName() == "BossEnemy" || pTarget->GetObjectName() == "BossBullet"|| pTarget->GetObjectName() == "Tiger")
	{
		KillMe();
	}

}
