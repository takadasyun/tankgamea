#pragma once
#include "Engine/GameObject.h"

//Tigerを管理するクラス
class Tiger : public GameObject
{
	int hModel_;    //モデル番号

	//移動処理
	//引数：なし
	//戻り値：なし
	void Move();

	//高さを地面に合わせる
	//引数：なし
	//戻り値：なし
	void FitHeightToGround();

public:
	//コンストラクタ
	Tiger(GameObject* parent);

	//デストラクタ
	~Tiger();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;
};