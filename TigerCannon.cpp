#include "TigerCannon.h"
#include "BossCannon.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "BossBullet.h"
#include "Tank.h"




//コンストラクタ
TigerCannon::TigerCannon(GameObject * parent)
	:GameObject(parent, "TigerCannon"), hModel_(-1)
{
}

//デストラクタ
TigerCannon::~TigerCannon()
{
}

//初期化
void TigerCannon::Initialize()
{
	hModel_ = Model::Load("TankTTCv1.fbx");
	assert(hModel_ >= 0); //assertは大事
	//Instantiate<BossCannon>(this);
}

//更新
void TigerCannon::Update()
{
	XMVECTOR flont = { 0,0,1,0 };
	XMMATRIX mat;


	//敵を探して、いなかったら 
	if (FindObject("Tank") == nullptr)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GameoverScene);
	}
	else
	{
		XMVECTOR flont = { 0,0,1,0 };
		XMMATRIX mat;

		Tank* pTank;
		pTank = (Tank*)FindObject("Tank");

		mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
		flont = XMVector3TransformCoord(flont, mat);

		XMVECTOR  mov = pTank->GetPosition() - transform_.position_;
		XMVECTOR  interval = pTank->GetPosition() - transform_.position_;

		mov = XMVector3Normalize(mov);
		flont = XMVector3Normalize(flont);
		float dot = XMVector3Dot(flont, mov).vecX;	//内積を求める	
		float angle = acos(dot);

		angle = XMConvertToDegrees(angle);



	

		XMMATRIX ro;
		ro = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));



		transform_.rotate_.vecY += angle / 50;

		float longitud = XMVector3Length(interval).vecX; //ベクトルの長さ


		if (Visual > angle && longitud < distance ) //視界に入ったら玉を出す
		{
			static int s_nTime;
			if (++s_nTime == 9)
			{
				s_nTime = -27;
				//発射
				BossBullet* pBossBullet = Instantiate<BossBullet>(GetParent()->GetParent()->GetParent());// (FindObject("PlayScene"))でもいいが、
																		//  いちいち探すのは、しんどい

				XMVECTOR shotPos = Model::GetBonePosition(hModel_, "TCShot");//砲台の先の骨を探す
				XMVECTOR CannonRoot = Model::GetBonePosition(hModel_, "TCRoot");//砲台の根っこの骨を探す

				pBossBullet->Shot(shotPos, shotPos - CannonRoot);  //shotPos - cannonRootでcannonRootから見たshotPosとの距離がわかる


			}


		}
	}
}



//描画
void TigerCannon::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void TigerCannon::Release()
{
}



