#pragma once
#include "Engine/GameObject.h"

//◆◆◆を管理するクラス
class TigerCannon : public GameObject
{
private:
	int hModel_;    //モデル番号
	const int Visual = 90; //視野の角度
	const int distance = 60; //視野の長さ

public:
	//コンストラクタ
	TigerCannon(GameObject* parent);

	//デストラクタ
	~TigerCannon();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;


	//描画
	void Draw() override;

	//開放
	void Release() override;

};