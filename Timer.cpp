#include "Timer.h"
#include "Engine/Image.h"

Timer::Timer(GameObject * parent)
{
	for (int i = 0; i < 31; i++)
	{
		hPict_[i] = -1;
	}
}

Timer::~Timer()
{
}

void Timer::Initialize()
{
	for (int i = 0; i < 31; i++)
	{
		switch (i)
		{
		case 0:
			hPict_[i] = Image::Load("timer00.png");
			break;
		case 1:
			hPict_[i] = Image::Load("timer01.png");
			break;
		case 2:
			hPict_[i] = Image::Load("timer02.png");
			break;
		case 3:
			hPict_[i] = Image::Load("timer03.png");
			break;
		case 4:
			hPict_[i] = Image::Load("timer04.png");
			break;
		case 5:
			hPict_[i] = Image::Load("timer05.png");
			break;
		case 6:
			hPict_[i] = Image::Load("timer06.png");
			break;
		case 7:
			hPict_[i] = Image::Load("timer07.png");
			break;
		case 8:
			hPict_[i] = Image::Load("timer08.png");
			break;
		case 9:
			hPict_[i] = Image::Load("timer09.png");
			break;
		case 10:
			hPict_[i] = Image::Load("timer10.png");
			break;
		case 11:
			hPict_[i] = Image::Load("timer11.png");
			break;
		case 12:
			hPict_[i] = Image::Load("timer12.png");
			break;
		case 13:
			hPict_[i] = Image::Load("timer13.png");
			break;
		case 14:
			hPict_[i] = Image::Load("timer14.png");
			break;
		case 15:
			hPict_[i] = Image::Load("timer15.png");
			break;
		case 16:
			hPict_[i] = Image::Load("timer16.png");
			break;
		case 17:
			hPict_[i] = Image::Load("timer17.png");
			break;
		case 18:
			hPict_[i] = Image::Load("timer18.png");
			break;
		case 19:
			hPict_[i] = Image::Load("timer19.png");
			break;
		case 20:
			hPict_[i] = Image::Load("timer20.png");
			break;
		case 21:
			hPict_[i] = Image::Load("timer21.png");
			break;
		case 22:
			hPict_[i] = Image::Load("timer22.png");
			break;
		case 23:
			hPict_[i] = Image::Load("timer23.png");
			break;
		case 24:
			hPict_[i] = Image::Load("timer24.png");
			break;
		case 25:
			hPict_[i] = Image::Load("timer25.png");
			break;
		case 26:
			hPict_[i] = Image::Load("timer26.png");
			break;
		case 27:
			hPict_[i] = Image::Load("timer27.png");
			break;
		case 28:
			hPict_[i] = Image::Load("timer28.png");
			break;
		case 29:
			hPict_[i] = Image::Load("timer29.png");
			break;
		case 30:
			hPict_[i] = Image::Load("timer30.png");
		}
	}
	transform_.position_ = { 0,0.85f,0,0 };
	transform_.scale_ = { 2.0f,2.0f,0,1 };
	time = 30;
}

void Timer::Update()
{
	timeBeginPeriod(1);	//時間計測の制度を上げる
	static DWORD lastFpsResetTime = timeGetTime();	//最後に表示させた時間
	DWORD nowTime = timeGetTime();//現在の時間
	if (nowTime - lastFpsResetTime > 1000)//1秒以上の差を計算
	{
		while (time == TimerFlag_)
		{
			switch (time)
			{
			case 30:TimerFlag_ = 30; break;
			case 29:TimerFlag_ = 29; break;
			case 28:TimerFlag_ = 28; break;
			case 27:TimerFlag_ = 27; break;
			case 26:TimerFlag_ = 26; break;
			case 25:TimerFlag_ = 25; break;
			case 24:TimerFlag_ = 24; break;
			case 23:TimerFlag_ = 23; break;
			case 22:TimerFlag_ = 22; break;
			case 21:TimerFlag_ = 21; break;
			case 20:TimerFlag_ = 20; break;
			case 19:TimerFlag_ = 19; break;
			case 18:TimerFlag_ = 18; break;
			case 17:TimerFlag_ = 17; break;
			case 16:TimerFlag_ = 16; break;
			case 15:TimerFlag_ = 15; break;
			case 14:TimerFlag_ = 14; break;
			case 13:TimerFlag_ = 13; break;
			case 12:TimerFlag_ = 12; break;
			case 11:TimerFlag_ = 11; break;
			case 10:TimerFlag_ = 10; break;
			case 9:TimerFlag_ = 9; break;
			case 8:TimerFlag_ = 8; break;
			case 7:TimerFlag_ = 7; break;
			case 6:TimerFlag_ = 6; break;
			case 5:TimerFlag_ = 5; break;
			case 4:TimerFlag_ = 4; break;
			case 3:TimerFlag_ = 3; break;
			case 2:TimerFlag_ = 2; break;
			case 1:TimerFlag_ = 1; break;
			case 0:TimerFlag_ = 0;
			}
			time--;
			TimerFlag_--;
			break;
		}
		lastFpsResetTime = nowTime;//最後に表示させた時間を入れる
	}

	if (nowTime == 0)
	{
		KillMe();
	}
}

void Timer::Draw()
{
	Image::SetTransform(hPict_[TimerFlag_], transform_);
	Image::Draw(hPict_[TimerFlag_]);
}

void Timer::Release()
{
}

void Timer::CountDown()
{
	time -= 1;
}
