#include "Turret.h"
#include "Cannon.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Bullet.h"
#include "BulletFar.h"
#include "Engine/Camera.h"



//コンストラクタ
Turret::Turret(GameObject * parent)
	:GameObject(parent, "Turret"), hModel_(-1)
{
}

//デストラクタ
Turret::~Turret()
{
}

//初期化
void Turret::Initialize()
{
	hModel_ = Model::Load("TankPTv2.fbx");
	assert(hModel_ >= 0); //assertは大事
	Instantiate<Cannon>(this);
}

//更新
void Turret::Update()
{

	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.rotate_.vecY += 1;
	}

	if (Input::IsKey(DIK_LEFT))
	{
		transform_.rotate_.vecY -= 1;
	}

}



//描画
void Turret::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Turret::Release()
{
}
